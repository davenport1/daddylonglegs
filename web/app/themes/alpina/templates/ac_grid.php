<?php
/**************************/
/**** AC Grid Template ****/ 
/**************************/

global $post;

// Excerpt
if ($show_excerpt) {

  // Apply a excerpt length if given
  if (isset($excerpt_length)) {
		$excerpt_length = intval($excerpt_length);  	  
  }
  else {
		$excerpt_length = false; // This will use the site default
  }
    
  // Hide read more link on some post types
  $show_read_more = true;
  if ($post_type == 'ac_gallery') {
	  $show_read_more = false;
  }

	// Add social icons to read more
	$excerpt = ac_get_excerpt($post, $excerpt_length, $show_read_more, true, ac_person_get_all_social_icons());
}

// Terms
$show_terms = true;
if (get_post_type() == 'ac_person') {
	$show_terms = false;
}
			
// Get terms slugs for Isotope filtering
$terms = ' all ';
$post_terms = get_the_terms( $post->ID, $post_category );

if(!empty($post_terms)) {
	foreach($post_terms as $post_term){
		$terms .= ' ' . $post_term->slug . ' ';
	}
}

// Get the post classes
$classes = implode(" ", get_post_class());
?>			

<div class='ac-grid-col <?php echo esc_attr($classes); ?> <?php echo $cols_class . esc_attr($terms) .esc_attr(ac_get_hide_until_fade_class()); ?>'>	
	<div class='ac-grid-post'>
	
	<?php
		// Write out the post
							
		// Check for content overrides
		// Testimonials and Post-Quotes use testimonial format
		if ( 	(get_post_type() == 'ac_testimonial') || 
					( (get_post_type() == 'post') && (get_post_format() == 'quote') )
				) {
			echo ac_testimonial_render($post->ID, true, $excerpt_length);
		}
		// Format a standard grid representation
		else { ?>

		<?php if ( ac_has_post_thumbnail($post->ID)) : ?>
			<div class='image'>				
				<a href="<?php echo get_permalink($post->ID); ?>" >
				<?php echo ac_resize_image_for_grid( ac_get_post_thumbnail_id($post->ID), $cols ); ?>
				</a>
			</div>
		<?php endif; ?>

		<?php if ( ($show_title) || ($show_excerpt) ) : ?>
		<div class='text'>
			<?php if ($show_title): ?><a href="<?php echo get_permalink($post->ID); ?>" ><h3 class='ac-grid-title'><?php echo get_the_title($post->ID); ?></h3></a><?php endif; ?>
			
		  <?php echo ac_person_get_position(); ?>
			
			<?php if ($show_terms) : ?>
			<div class='ac-grid-terms'><?php echo ac_get_the_term_list($post->ID, $post_category, '', ', ', ''); ?></div>
			<?php endif; ?>
			<?php if ($show_excerpt): ?><?php echo $excerpt; ?><?php endif; ?>
			
		</div>							
		<?php endif; ?>
		
	<?php } ?>
			
	</div>
</div>