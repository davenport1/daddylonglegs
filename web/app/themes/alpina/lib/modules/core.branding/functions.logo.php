<?php

if ( !function_exists( 'shoestrap_logo' ) ) :
/*
 * The site logo.
 * If no custom logo is uploaded, use the sitename
 */
function shoestrap_logo() {
  $logo  = shoestrap_getVariable( 'logo' );
  
  if ( !empty( $logo['url'] ) )
  {
	  // AC - resize the logo based on the header height
	  
	  // Use the header height to resize
	  $navbar_height = filter_var( shoestrap_getVariable( 'navbar_height', true ), FILTER_SANITIZE_NUMBER_INT );
	  $args = array(
	  	'current_height' => $logo['height'],
	  	'current_width' => $logo['width'],
	  	'new_height' => $navbar_height,
	  );

	  // Get the new image dimensions	  
		$resized = ac_resize_image_dimensions($args);
		
		// Resize the logo
	  $args = array(
	  	"crop" => false,
		  "height"     => $navbar_height,
		  "width"     => $resized['width'],
		  "url" => $logo['url'],
	  );
	  $image = shoestrap_image_resize( $args );
	  $image = $image['url'];
	    
    echo '<img id="site-logo" src="' . $image . '" alt="' . get_bloginfo( 'name' ) . '">';
  }
  else
    echo '<span class="sitename">' . bloginfo( 'name' ) . '</span>';
}
endif;

if ( !function_exists( 'shoestrap_branding_class' ) ) :
function shoestrap_branding_class( $echo = true ) {
  $logo  = shoestrap_getVariable( 'logo' );

  // apply the proper class
  $class = ( !empty( $logo['url'] ) ) ? 'logo' : 'text';

  // echo or return the value
  if ( $echo )
    echo $class;
  else
    return $class;
}
endif;
