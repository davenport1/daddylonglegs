<?php
/*******************************/
/****  Redux Pages Options  ****/ 
/*******************************/

if ( !function_exists( 'shoestrap_module_pages_options' ) ) :
function shoestrap_module_pages_options( $sections ) {

  $fields = array();
  // Pages
  $section = array(
    'title' => __( 'Pages', 'shoestrap' ),
    'icon' => 'el-icon-certificate icon-large'
  );
  
  // AC - Page Titles
  $fields[] = array( 
    'title'       => 'Page Titles',
    'desc'        => 'Set the default page title settings here.  You can override these on each page.',
    'id'          => 'help6',
    'default'     => __( '', 'shoestrap' ),
    'type'        => 'info'
  );  
  
  $fields[] = array( 
    'title'       => __( 'Page Title Background Color', 'shoestrap' ),
    'desc'        => __( '', 'shoestrap' ),
    'id'          => 'page_title_bg_color',
    'default'     => '#f3f3f3',
    'compiler'    => true,
    'customizer'  => array(),
    'transparent' => false,    
    'type'        => 'color'
  );  
  
  $fields[] = array( 
    'title'       => __( 'Page Title Padding', 'shoestrap' ),
    'desc'        => __( 'The amount of space above and below the page title', 'shoestrap' ),
    'id'          => 'page_title_padding',
    'default'   => 45,
    'min'       => 0,
    'step'      => 1,
    'edit'      => 1,    
    'max'       => 200,
    'type'      => 'slider',
  );    
  // - End / AC - Page Titles   
  
  $section['fields'] = $fields;
  
  $sections[] = $section;
  return $sections;
}
endif;
add_filter( 'redux-sections-'.REDUX_OPT_NAME, 'shoestrap_module_pages_options', 85 );