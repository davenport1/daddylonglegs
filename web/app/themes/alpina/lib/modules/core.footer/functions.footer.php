<?php

if ( !function_exists( 'shoestrap_footer_css' ) ) :
function shoestrap_footer_css() {
  $bg         = shoestrap_getVariable( 'footer_background' );
  $cl         = shoestrap_getVariable( 'footer_color' );
  $cl_brand   = shoestrap_getVariable( 'color_brand_primary' );
  $opacity = 1; // AC override due to feature removal
  $rgb        = shoestrap_get_rgb( $bg, true );
  $border     = shoestrap_getVariable( 'footer_border' );
  $top_margin = shoestrap_getVariable( 'footer_top_margin' );


  $style = 'footer.content-info {';

    $style .= ( $opacity != 1 && $opacity != "" ) ? 'background: rgba(' . $rgb . ',' . $opacity . ');' : 'background:' . $bg . ';';
    $style .= ( !empty($border) && $border['border-top'] > 0 && !empty($border['border-color']) ) ? 'border-top:' . $border['border-top'] . ' ' . $border['border-style'] . ' ' . $border['border-color'] . ';' : '';
    $style .= ( !empty($top_margin) ) ? 'margin-top:'. $top_margin .'px;' : '';
  $style .= '}';

  $style .= '#copyright-bar { line-height: 30px; }';
  $style .= '#footer_social_bar { line-height: 30px; font-size: 16px; text-align: right; }';
  $style .= '#footer_social_bar a { margin-left: 9px; padding: 3px; color:' . $cl . '; }';

  wp_add_inline_style( 'shoestrap_css', $style );
}
endif;


if ( !function_exists( 'shoestrap_footer_icon' ) ) :
/*
 * Creates the customizer icon on the bottom-left corner of our site
 * (visible only by admins)
 */
function shoestrap_footer_icon() {
  global $wp_customize;

  if ( current_user_can( 'edit_theme_options' ) && !isset( $wp_customize ) ) : ?>
    <div id="shoestrap_icon" class="visible-lg">
      <a href="<?php echo admin_url( 'themes.php?page=shoestrap' ); ?>"><i class="icon el-icon-cogs"></i></a>
    </div>
  <?php endif; 
}
endif;
add_action( 'shoestrap_after_footer', 'shoestrap_footer_icon' );

if ( !function_exists( 'shoestrap_footer_html' ) ) :
function shoestrap_footer_html() {
  $blog_name  = get_bloginfo( 'name', 'display' );
  $ftext      = shoestrap_getVariable( 'footer_text' );

  $ftext = ( $ftext == '' ) ? '&copy; [year] [sitename]' : $ftext;

  $ftext = str_replace( '[year]', date( 'Y' ), $ftext );
  $ftext = str_replace( '[sitename]', $blog_name, $ftext );

  $social = shoestrap_getVariable( 'footer_social_toggle' );
  $social_width = shoestrap_getVariable( 'footer_social_width' );

  $width = 12;

  // Social is enabled, we're modifying the width!
  $width = ( intval( $social_width ) > 0 && $social ) ? $width - intval( $social_width ) : $width;

  $social_blank = shoestrap_getVariable( 'footer_social_new_window_toggle' );

  $blank = ( $social_blank == 1 ) ? ' target="_blank"' : '';

  $networks = shoestrap_get_social_links();

  do_action( 'shoestrap_footer_before_copyright' );
  ?>

  <div id="footer-copyright">
    <article class="<?php echo shoestrap_container_class('footer-copyright'); ?>">
    	<div class='row'>
	      
          
          <div id="mc_embed_signup" class="col-lg-12">
	<div align="center" style="text-align: center; padding-top:30px; font-family: 'Didact Gothic', Arial, Helvetica, sans-serif; color:#fff; font-size:24px;"><strong>DAVENPORT THEATRE • 354 WEST 45TH STREET, NEW YORK, NY 10036</strong></div><img src="http://devdll.davenporttheatrical.com/app/uploads/2015/05/divider.jpg" style="padding-top:12px; padding-bottom:10px; margin: 0 auto 0; display:block;">
<div class="indicates-required"></div>
<div align="center"><form accept-charset="UTF-8" action="https://nk225.infusionsoft.com/app/form/process/f9692502e6533c7e2b0faff365b1753b" class="infusion-form" method="POST">
    <input name="inf_form_xid" type="hidden" value="f9692502e6533c7e2b0faff365b1753b" />
    <input name="inf_form_name" type="hidden" value="Sign up for newsletter" />
    <input name="infusionsoft_version" type="hidden" value="1.41.0.37" />
    <div class="infusion-field"></div>
    <div class="infusion-field">
      <label for="inf_field_Email" style="font-family: 'Lato', Arial, Helvetica, sans-serif; font-size: 20px; color: #fff;">SIGN UP TO HEAR AN EXCLUSIVE RECORDING OF "THE SECRET OF HAPPINESS"</label><br>
<input class="infusion-field-input-container" style="width:350px; border-radius:0px; margin-right:4px;" id="inf_field_Email" name="inf_field_Email" type="text"/>
        <span class="infusion-submit">
        <input type="submit" value="Submit" style="cursor: pointer; font-family: 'Lato', Arial, Helvetica, sans-serif;padding:10px; background-color:#d5e4e7; font-size:16px; border:2px solid #307988; color:#6a0028; -webkit-border-radius: 0px; border-radius: 0px; letter-spacing:0px"/>
        </span><br>
        <br>
    </div>
    <div class="infusion-submit"><span style="padding-top:16px"><a href="https://www.facebook.com/DaddyLongLegsTheMusical" target="_blank"><img src="http://devdll.davenporttheatrical.com/app/uploads/2015/05/facebook.jpg" width="38" height="38"></a> &nbsp;<a href="https://twitter.com/LongLegsMusical" target="_blank"><img src="http://devdll.davenporttheatrical.com/app/uploads/2015/05/twitter.jpg" width="38" height="38"></a> &nbsp;<a href="https://www.youtube.com/channel/UCWlhIV7R6_BIe1PlI9lG2vQ" target="_blank"><img src="http://devdll.davenporttheatrical.com/app/uploads/2015/05/youtube.jpg" width="38" height="38"></a></span></div>
</form>
<script type="text/javascript" src="https://nk225.infusionsoft.com/app/webTracking/getTrackingCode?trackingId=67e5baaa3791caa11d5c2de2b640f188"></script></div>
<br><div align="center" class="clear"><br>
</div>
    	</div></div>
    </article>
  </div>
  <?php
}
endif;

function shoestrap_footer_content() {
  // Finding the number of active widget sidebars
  $num_of_sidebars = 0;
  $base_class = 'col-md-';

  for ( $i=0; $i<5 ; $i++ ) {
    $sidebar = 'sidebar-footer-'.$i.'';
    if ( is_active_sidebar( $sidebar ) )
      $num_of_sidebars++;
  }

  // Showing the active sidebars
  for ( $i=0; $i<5 ; $i++ ) {
    $sidebar = 'sidebar-footer-' . $i;

    if ( is_active_sidebar( $sidebar ) ) {
      // Setting each column width accordingly
      $col_class = 12 / $num_of_sidebars;
    
      echo '<div class="' . $base_class . $col_class . '">';
      dynamic_sidebar( $sidebar );
      echo '</div>';
    }
  }

  // Showing extra features from /lib/modules/core.footer/functions.footer.php
  do_action( 'shoestrap_footer_pre_override' );
}
