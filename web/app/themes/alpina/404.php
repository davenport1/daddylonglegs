<?php
/**********************/
/**** 404 Template ****/ 
/**********************/
?>

<div class="contents">
	<h2><?php _e("Ooops! That page can't be found!"); ?></h2>
	<p><a href="<?php echo home_url(); ?>"><?php _e("Go back to start", 'alleycat')?></a> <?php _e("or try searching for what you were looking for.", 'alleycat'); ?></p>
	<?php get_search_form(); ?>
</div>