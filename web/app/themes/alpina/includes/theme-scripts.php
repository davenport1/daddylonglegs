<?php
/******************************************/
/****  Theme Scripts Access and Utils  ****/ 
/******************************************/

// --  Plugin Active Functions --
// Visual Composer
function ac_visual_composer_is_installed() {
	return class_exists('WPBakeryShortCode');
}

// Setup the VC
function ac_vc_before_init() {
	vc_set_as_theme(true); // Surpress all messages
}	


// Meta Box
function ac_meta_box_is_installed() {
	return function_exists('rwmb_meta');
}

// Icon Box
function ac_icon_box_is_installed() {
	return class_exists('VC_Icon_Box');
}

// == Pretty Photo ===
// Pretty Photo is included with Visual Composer

// Load Pretty Photo
function ac_load_prettyphoto() {

	if ( ac_visual_composer_is_installed() ) {
		// Setup lighbox for images
		wp_enqueue_script( 'prettyphoto' );
		wp_enqueue_style( 'prettyphoto' );		
	}
	
}

// Create and return  unique PrettyPhoto ID for this page load
function ac_get_prettyphoto_rel() {
	global $ac_pretty_photo_rel;
	
	// Create a unique PrettyPhoto ID for this page load
	if (! $ac_pretty_photo_rel) {
		$ac_pretty_photo_rel	= rand();
	}	
	
	return $ac_pretty_photo_rel;
}


// == End / Pretty Photo ===