<?php
/*************************/
/**** LESS Variables  ****/ 
/*************************/
// Theme LESS Varialbles that are included by the Shoestrap LESS compiler

// Returns the theme LESS Variables
function ac_get_theme_less_vars() {

	// Variables
	$page_title_bg_color      			= ac_prepare_bg_colour_for_less(shoestrap_getVariable( 'page_title_bg_color', true ) );
	$page_title_padding       			= filter_var( shoestrap_getVariable( 'page_title_padding', true ), FILTER_SANITIZE_NUMBER_INT );
	$button_bg_color      					= ac_prepare_bg_colour_for_less(shoestrap_getVariable( 'color_button', true ) );	
	$footer_color      							= ac_prepare_text_colour_for_less(shoestrap_getVariable( 'footer_color', true ) );	
	$footer_background      				= ac_prepare_bg_colour_for_less(shoestrap_getVariable( 'footer_background', true ) );	
	$aeis_slideshow_height					= shoestrap_getVariable( 'aeis_slideshow_height');	
	
	$vars = '
		@page-title-bg-color:         ' . $page_title_bg_color . ';
		@page-title-padding:         	' . $page_title_padding . 'px;
		@button-bg-color:				 			' . $button_bg_color . ';	
		@footer-color:				 				' . $footer_color . ';	
		@footer-background:				 		' . $footer_background . ';	
		@button-bg-text:             	lighten(@button-bg-color, 100%);
		@comment-author-bg-color:			darken(@body-bg, 5%);
		@aeis-slideshow-height:				 		' . $aeis_slideshow_height . 'px;	
	';
		
	return $vars;
	
}

// Prepares a background colour as a LESS variable
// Values shouldn't be blank as the LESS compiler expects something
// Default backgrounds to inherit
// $color = the hex value, can include #
function ac_prepare_bg_colour_for_less($colour) {
	
	if ($colour) {
		// Ensure we have a colour #
		return '#' . str_replace( '#', '', $colour );		
	}
	else {
		// Dont use inherit as mixins fall over
		// Get the page bg colour
		$page_bg = shoestrap_getVariable( 'html_color_bg' );
		// Backup, just in case
		if (!$page_bg) {
			$page_bg = '#ffffff';
		}
		
		return $page_bg;
	}

}

// Prepares a text colour as a LESS variable
// Values shouldn't be blank as the LESS compiler expects something
// Default texts to black
function ac_prepare_text_colour_for_less($colour) {
	
	if ($colour) {
		// Ensure we have a colour #
		return '#' . str_replace( '#', '', $colour );		
	}
	else {
		// We dont have a value, so return a default
		return "#000000";
	}

}