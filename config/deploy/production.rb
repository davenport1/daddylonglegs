set :stage, :production

# Simple Role Syntax
#  ==================
# role :app, %w{deploy@example.com}
# role :web, %w{deploy@example.com}
# role :db,  %w{deploy@example.com}

# Extended Server Syntax
# ======================

server '198.20.232.166', user: 'dll', roles: %w{web app db}

fetch(:default_env).merge!(wp_env: :production)

# set :wpcli_remote_url, "http://www.afewgoodmen.com/wp"
# set :wpcli_local_url, "http://localhost:8888/afewgoodmen-wp/web/wp"
